<%@page import="java.sql.DriverManager"%>
<%@page import="modeloCliente.Cliente"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Types"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="conexao.ConexaoBancoDeDados"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Delete Client</title>
    </head>
    <body>
        <%
          
            String deleteid = request.getParameter("id");
            Cliente c = new Cliente();
            c.setId(Integer.parseInt(deleteid));
            
            try{
                String sql_delete = "delete from usuario where id = ?";
                
                ConexaoBancoDeDados Conexion = new ConexaoBancoDeDados();

                // Conexao com todos os seus elementos
                Conexion.Conectar();

                //Statement
                PreparedStatement stmt = Conexion.getCon().prepareStatement(sql_delete);

                stmt.setInt(1, c.getId());
                stmt.execute();
                stmt.close();
                out.println("Deletado!");
                Conexion.getCon().close();
                        
            } catch (SQLException e) {
                out.println("Erro " + e);
            }

        %>
        
        <a href="index.html">Voltar para a página inicial</a>
    </body>
</html>
