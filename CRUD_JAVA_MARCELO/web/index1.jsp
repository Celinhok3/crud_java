<%-- 
    Document   : index
    Created on : 21/08/2019, 09:49:54
    Author     : Marcelo Ribeiro
--%>


<%@page import="java.sql.DriverManager"%>
<%@page import="modeloCliente.Cliente"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Types"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="conexao.ConexaoBancoDeDados"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consultando</title>
    </head>
    <body>

        <%
            //ConexaoBancoDeDados.Conectar();
            //out.println(ConexaoBancoDeDados.getCon());
            // String wid = request.getParameter("wid");
            String wnomes = request.getParameter("wnome");
            String email = request.getParameter("email");
            String tell = request.getParameter("tell");
            String cidade = request.getParameter("cidade");

            Cliente c = new Cliente();
            // c.setId(Integer.parseInt(wid));
            c.setNome(wnomes);
            c.setEmail(email);
            c.setTell(tell);
            c.setCidade(cidade);

            try {
                // cria um preparedStatement
                
                // Obs: Tem que alterar a tabela no SQL e colocar o auto_increment
                // Segue o código: alter table usuario modify id int not null auto_increment;
                String sql = "insert into usuario (id,nome,email,telefone,cidade) values (default,?,?,?,?)";
                ConexaoBancoDeDados Conexion = new ConexaoBancoDeDados();

                // Conexao com todos os seus elementos
                Conexion.Conectar();

                //Statement
                PreparedStatement stmt = Conexion.getCon().prepareStatement(sql);

                //preenche os valores                            
                //stmt.setInt(1, c.getId());
                stmt.setString(1, c.getNome());
                stmt.setString(2, c.getEmail());
                stmt.setString(3, c.getTell());
                stmt.setString(4, c.getCidade());
                // executa
                stmt.execute();
                stmt.close();
                out.println("Gravado!");
                Conexion.getCon().close();
            } catch (SQLException e) {
                out.println("Erro " + e);
            }


        %>

        <a href="index.html">Voltar para a página inicial</a>
    </body>
</html>
