<%-- 
    Document   : index3
    Created on : 21/08/2019, 20:23:15
    Author     : Marcelo Ribeiro
--%>
<%@page import="java.sql.DriverManager"%>
<%@page import="modeloCliente.Cliente"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Types"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="conexao.ConexaoBancoDeDados"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update Client</title>
    </head>
    <body>
        <h1>Update User</h1>
        <%
            
            String up_id = request.getParameter("up_id");
            String upnome = request.getParameter("upnome");
            String upemail = request.getParameter("upemail");
            String uptelefone = request.getParameter("uptelefone");
            String upcidade = request.getParameter("upcidade");
            /*
            String up_nome = "up_nome";
            String up_email = "up_email";
            String up_telefone = "up_telefone";
            String up_cidade = "up_cidade";
            */
            Cliente c = new Cliente();
            

                c.setNome(request.getParameter("upnome"));
                c.setEmail(request.getParameter("upemail"));
                c.setTell(request.getParameter("uptelefone"));
                c.setCidade(request.getParameter("upcidade"));
                
                String vazio = "";

            try {
                //Conexão com Banco de dados
                ConexaoBancoDeDados Conexion = new ConexaoBancoDeDados();

                Conexion.Conectar();

                if (up_id != vazio) {
                    c.setId(Integer.parseInt(up_id));
                        
                    
                    
                    if (upnome != vazio) {
                        String sql_nome = "update usuario set nome = ? where id = ? ";
                        PreparedStatement stmt = Conexion.getCon().prepareStatement(sql_nome);
                        stmt.setString(1, c.getNome());
                        stmt.setInt(2, c.getId());
                        stmt.executeUpdate();
                        stmt.close();
                        out.println("<p></br>Nome alterado com sucesso!!</p>");
                    }

                    if (upemail != vazio) {
                        String sql_email = "update usuario set email = ? where id = ?";
                        PreparedStatement stmt = Conexion.getCon().prepareStatement(sql_email);
                        stmt.setString(1, c.getEmail());
                        stmt.setInt(2, c.getId());
                        stmt.executeUpdate();
                        stmt.close();
                        out.println("</br><p>Email alterado com sucesso!!</p>");
                    }

                    if (uptelefone != vazio) {
                        String sql_telefone = "update usuario set telefone = ? where id = ?";
                        PreparedStatement stmt = Conexion.getCon().prepareStatement(sql_telefone);
                        stmt.setString(1, c.getTell());
                        stmt.setInt(2, c.getId());
                        stmt.executeUpdate();
                        stmt.close();
                        out.println("</br><p>Telefone alterado com sucesso!!</p>");
                    }

                    if (upcidade != vazio) {
                        String sql_cidade = "update usuario set cidade = ? where id = ?";
                        PreparedStatement stmt = Conexion.getCon().prepareStatement(sql_cidade);
                        stmt.setString(1, c.getCidade());
                        stmt.setInt(2, c.getId());
                        stmt.executeUpdate();
                        stmt.close();
                        out.println("</br><p>Cidade alterada com sucesso!!</p>");
                    } else {
                        out.println("O seu ID está nulo, tente novamente");
                        response.sendRedirect("index.html");

                    }
                }
            } catch (Exception e) {
                out.println("erro ai selecionado dados" + e);
            }
            // String sql_email = "update usuario set ='"+up_email+"'where id = '"+up_id+"'";
            // 
            // 
        %>
        </br>
        <a href="index.html">Voltar para a página inicial</a>
    </body>
</html>
