/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author celsosilva
 */
public class ConexaoBancoDeDados {

    private String sql;
    private PreparedStatement stmt;
    private static Connection con = null;

    public static Connection getCon() {
        return con;
    }

    public static String getSql(String sql) {
        return sql;
    }

    public static Connection getStmt(String sql, PreparedStatement stmt) throws SQLException {
        stmt = con.prepareStatement(sql);
        return (Connection) stmt;

    }

    public ConexaoBancoDeDados() {
    } //Possibilita instancias

    public static void Conectar() {

        System.out.println("Conectando ao banco...");

        try {
            Class.forName("com.mysql.jdbc.Driver");

            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/banco_crud", "root", "");

            System.out.println("Conectado.");

        } catch (ClassNotFoundException ex) {
            System.out.println("Classe não encontrada, adicione o driver nas bibliotecas.");
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);

        } catch (SQLException e) {
            System.out.println(e);
            throw new RuntimeException(e);
        }

    }

    List listaAlunos(Object object) {
        throw new UnsupportedOperationException("Alunos estudarem este metodo."); //To change body of generated methods, choose Tools | Templates.
    }

    private static class Conexao {

        public Conexao() {
        }
        // ele permite instacia, utilizar todos os metodos que vem da classe.
    }

}
